package com.deloitte.ConfluenceClient.lucapino;

import com.deloitte.ConfluenceClient.Response;
import com.github.lucapino.confluence.rest.client.api.ClientFactory;
import com.github.lucapino.confluence.rest.client.api.ContentClient;
import com.github.lucapino.confluence.rest.client.impl.ClientFactoryImpl;
import com.github.lucapino.confluence.rest.core.api.domain.content.ContentBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.StorageBean;
import com.github.lucapino.confluence.rest.core.api.misc.ExpandField;
import com.github.lucapino.confluence.rest.core.api.misc.SecurityException;
import com.github.lucapino.confluence.rest.core.impl.APIUriProvider;
import com.github.lucapino.confluence.rest.core.impl.HttpAuthRequestService;
import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Value("${confluence.url}")
    private String confluenceURL;

    @Value("${confluence.user}")
    private String confluenceUser;

    @Value("${confluence.password}")
    private String confluencePassword;

    @GetMapping("sayHi")
    @ApiOperation(value = "just a check if service say hi")
    public Response sayHi() {
        log.info("Incoming call on method: sayHi()");
        return new Response("test");
    }

    @ApiOperation(value = "Endpoint to interact directly with Rule Engine. Passing rules and facts")
    @PostMapping("/getContentByPageId")
    public String getContentByPageId(@RequestBody String pageId) {
        log.info("Incoming call on method: getContentByPageId()");

        String result = "";

        HttpAuthRequestService requestService = new HttpAuthRequestService();
        try {
            requestService.connect(new URI(confluenceURL), confluenceUser, confluencePassword);

            ExecutorService executorService = Executors.newCachedThreadPool();
            APIUriProvider uriProvider = new APIUriProvider(new URI(confluenceURL + "/wiki"));
            ClientFactory factory = new ClientFactoryImpl(executorService, requestService, uriProvider);

            ContentClient contentClient = factory.getContentClient();
            List<String> expand = new ArrayList<>();
            expand.add(ExpandField.BODY_VIEW.getName());
            expand.add(ExpandField.BODY_STORAGE.getName());
            expand.add(ExpandField.VERSION.getName());
            expand.add(ExpandField.SPACE.getName());
            Future<ContentBean> future = contentClient.getContentById(pageId, 0, expand);
            ContentBean contentBean = future.get();

            result = contentBean.getBody().getView().getValue();
            System.out.println(contentBean.getBody().getView().getValue());

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    @ApiOperation(value = "Endpoint to interact directly with Rule Engine. Passing rules and facts")
    @PostMapping("/updateContent")
    public String updateContent(@RequestParam String pageId, @RequestParam String content) {
        log.info("Incoming call on method: updateContent()");

        // ***
        // *** NOT WORKING ***
        // ***
        String result = "";

        HttpAuthRequestService requestService = new HttpAuthRequestService();
        ContentBean contentBean = null;
        try {
            requestService.connect(new URI(confluenceURL), confluenceUser, confluencePassword);

            ExecutorService executorService = Executors.newCachedThreadPool();
            APIUriProvider uriProvider = new APIUriProvider(new URI(confluenceURL + "/wiki"));
            ClientFactory factory = new ClientFactoryImpl(executorService, requestService, uriProvider);

            ContentClient contentClient = factory.getContentClient();
            List<String> expand = new ArrayList<>();
            expand.add(ExpandField.BODY_VIEW.getName());
            expand.add(ExpandField.BODY_STORAGE.getName());
            expand.add(ExpandField.SPACE.getName());
            Future<ContentBean> future = contentClient.getContentById(pageId, 0, expand);
            contentBean = future.get();

            String contentValue = contentBean.getBody().getView().getValue();
            System.out.println(contentValue);

            contentBean.getBody().getView().setValue("lol");
            contentBean.getBody().getStorage().setValue("lol");
            Future<StorageBean> newContent = contentClient.convertContent(contentBean.getBody().getStorage());
            contentBean.getBody().setStorage(newContent.get());
            future = contentClient.updateContent(contentBean);
            contentBean = future.get();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

}
