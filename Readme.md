# Confluence Client

This application wants to expose a simple way to interact with Confluence, hiding the complexity of a http communication.
This service will expose REST endpoint to facilitate the interaction with Confluence API.

## Code Standards
- install intelliJ plugin "SequenceDiagram". This helps with auto generation of app diagrams
- install IntelliJ plugin "PlantUML Integration". This help to create diagrams from script
- if it is needed a diagram difficult to generate, use Draw.io (https://app.diagrams.net/)

## References

Confluence REST API Specification (OpenAPI):  
https://developer.atlassian.com/cloud/confluence/swagger.v3.json

Atlassian Confluence 6.8.0 JavaDoc:  
https://docs.atlassian.com/ConfluenceServer/javadoc/6.8.0/?_ga=2.136506032.1443004396.1532937685-475902104.1531735149

Confluence Server REST API:  
https://developer.atlassian.com/server/confluence/confluence-server-rest-api/

Confluence REST API Example:  
https://developer.atlassian.com/server/confluence/confluence-rest-api-examples/

Simple Java client:  
https://github.com/lucapino/confluence-rest-client

### Generating code from OPEN API specification
- Tutorial code generation from API Spec: https://slacker.ro/2021/09/07/generating-a-java-rest-client-for-confluence-cloud/
- Tutorial code generation from API Spec: https://www.baeldung.com/java-openapi-generator-server

### Running Confluence in local
Follow the link below at the Docker section. Once the app run, on localhost:8090 is present the interface of Confluence but it is needed an activation key.
https://hub.docker.com/r/atlassian/confluence-server/

### Packages
the idea is to have different initial implementation (PoC) and then choose the best one for our needs.

here the organisation of the packaging (at the moment)
- SwaggerCodeGen
- Client Library (lucapino)
- ConfluenceAPI (java library)
- ConfluenceREST (HTTP endpoints)

#### SwaggerCodeGen
generate code from OpenAPI specification through Swagger codegen. This will create a java application that maps the specification of api.

PRO:
  - instant generation of a library
  - all endpoint mapped

CON:
  - high volume of code unused until mapped/managed
  - weak documentation

#### Client Library
There is a library written by LucaPino on github that has a tiny set of functions that looks enough for our idea (create page, update page, attach file)

PRO:
- a small footprint of the library
- quick solution 

CON:
- weak documentation


#### ConfluenceREST
This process means that we will write a library to interact with Confluence, then the app that use the library to create and update pages
It is needed a client layer that call through HTTP the confluenceAPI layer, wrap everything in some Java object and expose it to the service classes.

PRO:
- nice implementation to do
- control over the library

CON:
- slow process in building the app
- why replicate something existing? 


#### ConfluenceAPI
This is a Java library that Confluence released, there is the javadoc available but not so much documentation and tutorials on how to use it.

PRO:
- Official library given by Confluence
- Expose all functionalities

CON:
- high volume of code unused until mapped/managed
- weak documentation 

